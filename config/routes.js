import { UserOutlined, ChromeOutlined, SettingOutlined } from '@ant-design/icons';

export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './user/Login',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/account/setting',
    name: '个人设置',
    component: './account/setting',
  },
  {
    path: '/welcome',
    name: '欢迎',
    icon: 'smile',
    component: './welcome/index',
  },
  {
    path: '/admin',
    name: 'admin',
    icon: 'crown',
    access: 'canAdmin',
    component: './Admin',
    routes: [
      {
        path: '/admin/sub-page',
        name: 'sub-page',
        icon: 'smile',
        component: './Welcome',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/members',
    name: '会员管理',
    icon: 'UserOutlined',
    routes: [
      {
        path: '/members/member',
        name: '会员管理',
        icon: 'smile',
        component: './members/member',
      },
      {
        path: '/members/memberLog',
        name: '会员日志',
        icon: 'smile',
        component: './members/memberLog',
      },
    ],
  },
  {
    path: '/cms',
    name: '内容管理',
    icon: 'ChromeOutlined',
    routes: [
      {
        path: '/cms/category',
        name: '内容分类',
        icon: 'smile',
        component: './cms/category',
      },
      {
        path: '/cms/content',
        name: '内容管理',
        icon: 'smile',
        component: './cms/content',
      },
      {
        path: '/cms/comment',
        name: '留言管理',
        icon: 'smile',
        component: './cms/comment',
      },
      {
        path: '/cms/setting',
        name: '内容设置',
        icon: 'smile',
        component: './cms/setting',
      },
    ],
  },
  {
    path: '/setting',
    name: '设置管理',
    icon: 'SettingOutlined',
    routes: [
      {
        path: '/setting/base',
        name: '基础设置',
        icon: 'smile',
        component: './setting/base',
      },
      {
        path: '/setting/api',
        name: '接口设置',
        icon: 'smile',
        component: './setting/api',
      },
      {
        path: '/setting/wechat',
        name: '微信设置',
        icon: 'smile',
        component: './setting/wechat',
      },
      {
        path: '/setting/account',
        name: '个人设置',
        component: './account/setting',
      },
    ],
  },
  {
    path: '/rule',
    name: '权限管理',
    icon: 'SettingOutlined',
    routes: [
      {
        path: '/rule/menu',
        name: '菜单管理',
        icon: 'smile',
        component: './rule/menu',
      },
      {
        path: '/rule/role',
        name: '角色管理',
        icon: 'smile',
        component: './rule/role',
      },
      {
        path: '/rule/user',
        name: '用户管理',
        icon: 'smile',
        component: './rule/user',
      },
      {
        path: '/rule/userLog',
        name: '用户日志',
        icon: 'smile',
        component: './rule/userLog',
      },
    ],
  },
  {
    path: '/system',
    name: '系统管理',
    icon: 'SettingOutlined',
    routes: [
      {
        path: '/system/setting',
        name: '设置管理',
        icon: 'smile',
        component: './system/setting',
      },
      {
        path: '/system/apidoc',
        name: '接口文档',
        icon: 'smile',
        component: './system/apidoc',
      },
    ],
  },
  {
    path: '/',
    redirect: '/welcome',
  },
  {
    component: './404',
  },
];
