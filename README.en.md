<h1 align="center">Mi Admin</h1>

<div align="center">
基于ThinkPHP6和React的极简后台管理系统
</div>

![image](./document/img/home.png)

- 使用文档：https://miren123.gitee.io/mi-admin-docs
- 国内镜像：https://gitee.com/miren123/mi-admin-react
- 后端地址：https://gitee.com/miren123/mi-admin-manage

## 使用

### clone

```bash
$ git clone https://gitee.com/miren123/mi-admin-react
```

### yarn

```bash
$ yarn install
$ yarn serve
```

### or npm

```bash
$ npm install
$ npm run serve
```

### 安装数据库

![image](./document/img/install.png)

更多信息参考 [使用文档](https://miren123.gitee.io/mi-admin-docs)
