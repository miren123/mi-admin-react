import React, {useRef, useState} from 'react';
import { GridContent } from '@ant-design/pro-layout';
import ProForm, {ProFormText, ProFormRadio} from '@ant-design/pro-form';
import { message, Form, Upload, Image, Button, Card, Col, Row, Avatar} from 'antd';
import { getOffiInfo, offiEdit } from "@/services/setting";
import { UploadOutlined, UserOutlined } from '@ant-design/icons';

import styles from "../style.less";

const Index = () => {
  const [subLoading, setSubLoading] = useState(false)
  const [qrcodeUrl, setQrcodeUrl] = useState('');
  const [qrcodePath, setQrcodePath] = useState('');
  const [visible, setVisible] = useState(false);

  const actionRef = useRef();

  const [formLayout] = useState('horizontal');

  const formItemLayout = formLayout === 'horizontal' ? { labelCol: {span: 4}, wrapperCol: {span: 14},} : null;

  /**
   * 作者：糜家智
   * 时间：2022/5/17
   * 功能：提交数据
   */
  const handleSubmit = async (values) => {
    setSubLoading(true)
    const params = values;
    params.qrcode = qrcodePath

    const res = await offiEdit(params)

    if (res.code === 200) {
      setSubLoading(false)
      message.success(res.msg);
    }
  };

  /**
   * 作者：糜家智
   * 时间：2022/5/17
   * 功能：获取数据
   */
  const getData = async () => {
    const res = await getOffiInfo()
    setQrcodeUrl(res.data.qrcode_url)
    return res.data
  };

  /**
   * 作者：糜家智
   * 时间：2022/5/17
   * 功能：头像配置
   */
  const avatarProps = {
    name: 'file',
    action: '/admin/SettingWechat/qrcode',
    headers: {
      AdminToken: localStorage.getItem('access_token') || '',
    },
    data: {
      type: 'offi'
    },
    accept: '.jpg,.png,.jpeg',
    showUploadList: false,
    beforeUpload(file) {
      if (file.size > 102400 * 5) {
        message.warning('请上传500k以下的二维码~');
        return false;
      }
    },
    onChange(info) {
      if (info.file.status === 'done') {
        let res = info.file.response;
        if (res.code == 200) {
          message.success('二维码上传成功~');
          setQrcodeUrl(res.data.file_url);
          setQrcodePath(res.data.file_path);
        } else {
          message.error(res.msg);
        }
      } else if (info.file.status === 'error') {
        message.error('二维码上传失败！');
      }
    },
  };

  return (
    <GridContent >
      <Row gutter={24}>
        <Col lg={24} md={24}>
          <Card
            bordered={false}
            style={{
              marginBottom: 24,
            }}
          >
            <ProForm
              formRef={actionRef}
              onFinish={(values) => handleSubmit(values)}
              request={() => getData()}
              {...formItemLayout}
              layout={formLayout}
              submitter={false}
            >

              <Form.Item label="二维码">
                <div className={styles.editAvatar}>
                  <Avatar
                    size={110}
                    icon={<UserOutlined />}
                    src={qrcodeUrl}
                    onClick={() => setVisible(true)}
                  />

                  <Image
                    width={200}
                    style={{ display: 'none' }}
                    preview={{
                      visible,
                      src: qrcodeUrl,
                      onVisibleChange: (value) => {
                        setVisible(value);
                      },
                    }}
                  />

                  <Upload {...avatarProps}>
                    <Button className={styles.btnUpload} icon={<UploadOutlined />}>
                      单击上传
                    </Button>
                  </Upload>

                </div>
              </Form.Item>

              <ProFormText name="name" label="名称" placeholder="请输入名称"
                           rules={[{required: true, message: '名称不能为空'}]}
              />
              <ProFormText name="origin_id" label="原始ID" placeholder="请输入原始ID"/>
              <ProFormText name="appid" label="AppID" placeholder="请输入AppID"
                           rules={[{required: true, message: 'AppID不能为空'}]}
              />
              <ProFormText name="appsecret" label="AppSecret" placeholder="请输入AppSecret"
                           rules={[{required: true, message: 'AppSecret不能为空'}]}
              />
              <ProFormText name="token" label="令牌(Token)" placeholder="请输入令牌(Token)"
                           rules={[{required: true, message: '令牌(Token)不能为空'}]}
              />
              <ProFormText name="encoding_aes_key" label="消息加解密密钥" placeholder="请输入消息加解密密钥"
                           rules={[{required: true, message: '消息加解密密钥不能为空'}]}
              />
              <ProFormRadio.Group
                name="encoding_aes_type"
                label="消息加解密方式"
                options={[
                  {
                    label: '明文模式',
                    value: 1,
                  },
                  {
                    label: '兼容模式',
                    value: 2,
                  },
                  {
                    label: '安全模式',
                    value: 3,
                  },
                ]}
              />
              {/*<ProFormText name="encoding_aes_type" label="消息加解密方式"*/}
              {/*             rules={[{required: true, message: '消息加解密方式不能为空'}]}*/}
              {/*/>*/}
              <Form.Item wrapperCol={{ offset: 2 }}>

                <Button type="primary" htmlType="submit" loading={subLoading}>
                  提交
                </Button>

              </Form.Item>

            </ProForm>

          </Card>
        </Col>
      </Row>

    </GridContent>
  );
};

export default Index;
