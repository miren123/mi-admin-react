import React, {useState, useRef, useLayoutEffect} from 'react';
import {PageContainer, GridContent} from '@ant-design/pro-layout';
import {Menu} from 'antd';
import OfficialView from './components/official';
import MiniView from './components/mini';
import styles from './style.less';

const {Item} = Menu;

const Index = () => {
  const menuMap = {
    mini: '小程序',
    official: '公众号',
  }

  const [initConfig, setInitConfig] = useState({
    mode: 'inline',
    selectKey: 'mini',
  })

  const dom = useRef()

  const resize = () => {
    requestAnimationFrame(() => {
      if (!dom.current) {
        return;
      }
      let mode = 'inline';
      const {offsetWidth} = dom.current;
      if (dom.current.offsetWidth < 641 && offsetWidth > 400) {
        mode = 'horizontal';
      }
      if (window.innerWidth < 768 && offsetWidth > 400) {
        mode = 'horizontal';
      }
      setInitConfig({...initConfig, mode: mode});
    });
  };

  useLayoutEffect(() => {
    if (dom.current) {
      window.addEventListener('resize', resize);
      resize();
    }
    return () => {
      window.removeEventListener('resize', resize);
    };
  }, [dom.current]);

  const getMenu = () => {
    return Object.keys(menuMap).map((item) => <Item key={item}>{menuMap[item]}</Item>);
  };

  const renderChildren = () => {
    const {selectKey} = initConfig;
    switch (selectKey) {
      case 'mini':
        return <MiniView/>;
      case 'official':
        return <OfficialView/>;
      default:
        return null;
    }
  };

  return (
    <PageContainer>
      <GridContent>
        <div
          className={styles.main}
          ref={(ref) => {
            if (ref) {
              dom.current = ref;
            }
          }}
        >
          <div className={styles.leftMenu}>
            <Menu
              mode={initConfig.mode}
              selectedKeys={[initConfig.selectKey]}
              onClick={({key}) => {
                setInitConfig({
                  ...initConfig,
                  selectKey: key,
                });
              }}
            >
              {getMenu()}
            </Menu>
          </div>
          <div className={styles.right}>
            <div className={styles.title}>{menuMap[initConfig.selectKey]}</div>
            {renderChildren()}
          </div>
        </div>
      </GridContent>
    </PageContainer>
  );
};
export default Index;
