import React, {useRef, useState} from 'react';
import ProTable from "@ant-design/pro-table";
import {Button, message, Space, Popconfirm } from "antd";
import {PageContainer} from '@ant-design/pro-layout';
import {PlusOutlined} from "@ant-design/icons";
import {getApiList, delApi} from '@/services/setting';
import Edit from "./components/Edit";

const Index = () => {
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [editData, setEditData] = useState(null)
  const [cateData, setCateData] = useState(null)

  const actionRef = useRef();

  /**
   * 获取数据
   * 作者：糜家智
   * 时间：2021/8/2 18:03
   */
  const getData = async () => {
    const res = await getApiList()
    setCateData(res.data.list)
    return {
      data: res.data.list,
      success: true,
      total: res.data.count
    }
  }

  /**
   * 用户的启用和禁用
   * @param {*} uid
   */
  const handleCategoryLock = async (data, hide) => {
    const setHide = hide === 0 ? '1' : '0';
    const arr = [];
    arr[0] = data;
    const res = await isHide({category: arr, is_hide: setHide})
    if (res.code === 200) {
      message.success(res.msg)
    }
  }

  /**
   * 模态框的显示和隐藏
   */
  const isShowModal = (show, data = null) => {
    setEditData(data)
    setIsModalVisible(show)
  }

  /**
   * 删除列
   * @param id
   * @returns {Promise<void>}
   */
  const handleOk = async (id) => {
    const res = await delApi({api_id: id})
    if (res.code === 200){
      actionRef.current.reload()
      message.success(res.msg)
    }

  };

  /**
   * 取消删除弹窗
   */
  const handleCancel = () => {
    message.success('取消了删除')
  };

  const columns = [
    {
      title: '接口名称',
      dataIndex: 'api_name',
      key: 'api_name',
    },
    {
      title: '接口链接',
      dataIndex: 'api_url',
      key: 'api_url',
    },
    {
      title: '接口排序',
      dataIndex: 'api_sort',
      key: 'api_sort',
    },
    {
      title: '接口ID',
      dataIndex: 'api_id',
      key: 'api_id',
    },
    {
      title: '接口PID',
      dataIndex: 'api_pid',
      key: 'api_pid',
    },
    {
      title: '添加时间',
      dataIndex: 'create_time',
      key: 'create_time',
    },
    {
      title: '修改时间',
      dataIndex: 'update_time',
      key: 'update_time',
    },
    {
      title: '操作',
      render: (_, record) => (
        <Space size="middle">
          <a onClick={() => isShowModal(true, record)}>编辑</a>
          <Popconfirm
            title="确定删除么？"
            onConfirm={() => {
              handleOk(record.category_id);
            }}
            onCancel={handleCancel}
            okText="确定"
            cancelText="取消"
          >
            <a href={"javascript"}>
              删除
            </a>
          </Popconfirm>
        </Space>
      ),
      hideInSearch: true
    },
  ];

  return (
    <PageContainer>
      <ProTable
        columns={columns}
        actionRef={actionRef}
        request={() => getData()}
        // dataSource={data}
        editable={{
          type: 'multiple',
        }}
        rowKey="api_id"
        search={false}
        pagination={false}
        dateFormatter="string"
        headerTitle="接口列表"
        toolBarRender={() => [
          <Button key="button" icon={<PlusOutlined/>} type="primary" onClick={() => isShowModal(true)}>
            新建
          </Button>,
        ]}
      />
      {
        !isModalVisible ? '' :
          <Edit
            isModalVisible={isModalVisible}
            isShowModal={isShowModal}
            actionRef={actionRef}
            editData={editData}
            cateData={cateData}
          />
      }

    </PageContainer>
  );
};

export default Index;
