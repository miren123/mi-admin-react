import React from 'react';
import {PageContainer} from '@ant-design/pro-layout';
import {Button, Tabs, Form, Input, Checkbox, Switch, Row, Col, Space, message} from 'antd';
import './index.less'

const Index = () => {

  const { TabPane } = Tabs;

  function callback(key) {
    console.log(key);
  }

  const onFinish = () => {
    setTimeout(function () {
      message.warn('你没有权限操作：接口设置修改')
    }, 500)
  };

  const refresh = () => {
    setTimeout(function () {
      message.success('操作成功')
    }, 500)
  }

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <PageContainer>
      <Tabs onChange={callback} type="card" className="tab-con">
        <TabPane tab="验证码设置" key="1">

          <Form
            name="basic"
            labelCol={{ span: 2 }}
            wrapperCol={{ span: 6 }}
            // initialValues={{}}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              label="注册验证码"
              name="username"
            >
              <Switch checked onChange={() => {}} />
            </Form.Item>

            <Form.Item
              label="登录验证码"
              name="password"
            >
              <Switch checked={false} onChange={() => {}} />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 2, span: 6 }}>
              <Button onClick={refresh}>
                刷新
              </Button>

              <Button className="sub-btn" type="primary" htmlType="submit">
                提交
              </Button>
            </Form.Item>
          </Form>
        </TabPane>
        <TabPane tab="日志设置" key="2">
          <Form
            name="basic"
            labelCol={{ span: 2 }}
            wrapperCol={{ span: 6 }}
            // initialValues={{ log: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
          >
            <span className="log-title">开启后，会记录前台会员日志。</span>

            <Form.Item
              label="日志记录"
              name="log"
            >
              <Switch checked onChange={() => {}} />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 2, span: 6 }}>
              <Button onClick={refresh}>
                刷新
              </Button>

              <Button className="sub-btn" type="primary" htmlType="submit">
                提交
              </Button>
            </Form.Item>
          </Form>
        </TabPane>
        <TabPane tab="接口设置" key="3">
          <Form
            name="basic"
            labelCol={{ span: 2 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ su: 3 }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
          >
            <span className="log-title">次数/时间；3/1：3次1秒；次数设置为 0 则不限制。</span>

            <Form.Item
              label="接口速率"
              name="su"
            >
                <Input addonAfter="秒" className="api-item" />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 2, span: 6 }}>
              <Button onClick={refresh}>
                刷新
              </Button>

              <Button className="sub-btn" type="primary" htmlType="submit">
                提交
              </Button>
            </Form.Item>
          </Form>
        </TabPane>
      </Tabs>
    </PageContainer>
  );
};

export default Index;
