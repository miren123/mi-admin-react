import React, {useRef} from 'react';
import {message} from 'antd';
import ProForm, {ProFormText} from "@ant-design/pro-form";
import {editAdminUserPwd} from '@/services/ant-design-pro/api';

const SecurityView = () => {
  const formRef = useRef();

  /**
   * 作者：糜家智
   * 时间：2022/04/18 17:32:00
   * 功能：提交按钮
   */
  const handleSubmit = async (values) => {
    const data = values;
    const res = await editAdminUserPwd(data);
    if (res.code === 200) {
      message.success(res.msg)
    }
    formRef.current.resetFields()
  };

  return (
    <>
      <ProForm
        onFinish={(values) => handleSubmit(values)}
        formRef={formRef}
        layout="vertical"
        submitter={{
          resetButtonProps: {
            style: {
              display: 'none',
            },
          },
          submitButtonProps: {
            children: '修改密码',
          },
        }}
        hideRequiredMark
      >

        <ProFormText.Password width="md" name="password_old" label="旧密码" placeholder="请输入旧密码" rules={[{required: true, message: '请输入旧密码'}]}/>
        <ProFormText.Password width="md" name="password_new" label="新密码" placeholder="请输入新密码" rules={[{required: true, message: '请输入新密码'}]}/>

      </ProForm>
    </>
  );
};

export default SecurityView;
