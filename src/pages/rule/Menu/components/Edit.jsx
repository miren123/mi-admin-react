import React, {useState} from 'react';
import ProForm, {ProFormText} from '@ant-design/pro-form';
import {Modal, message, Form, Cascader, Radio} from 'antd';
import {getMenuInfo, editMenu, addMenu} from '@/services/rule';

const Edit = (props) => {
  const {isModalVisible, isShowModal, actionRef, editId, cateData} = props;

  const [formLayout] = useState('horizontal');

  const title = editId === null ? '添加菜单' : '编辑菜单';

  const formItemLayout =
    formLayout === 'horizontal'
      ? {
        labelCol: {span: 5},
        wrapperCol: {span: 19},
      }
      : null;

  /**
   * 作者：糜家智
   * 时间：2022/04/23 17:20:29
   * 功能：获取数据
   */
  const getData = async () => {
    const res = await getMenuInfo({
      admin_menu_id: editId,
    });
    res.data.pidStr = res.data.pidStr === false ? null : res.data.pidStr.split(',').map(Number);
    return res.data;
  };

  /**
   * 提交按钮
   * @param values
   * @returns {Promise<void>}
   */
  const handleSubmit = async (values) => {
    const data = values;
    let res = {};

    // 菜单父级
    data['menu_pid'] = data.pidStr ? data.pidStr[data.pidStr.length - 1] : 0

    if (editId === null) {
      res = await addMenu(data);
    } else {
      data.admin_menu_id = editId;
      res = await editMenu(data);
    }

    if (res.code === 200) {
      message.success(res.msg);
      actionRef.current.reload();
      isShowModal(false);
    }
  };

  // 递归修改data属性值，配合treeSelect规范数据
  const handleData = (data) => {
    let item = [];
    data.map((list) => {
      let newData = {};
      newData.value = list.api_id;
      newData.title = list.api_name;
      newData.children = list.children ? handleData(list.children) : []; // 如果还有子集，就再次调用自己
      item.push(newData);
    });
    return item;
  };

  return (
    <Modal
      title={title}
      visible={isModalVisible}
      onCancel={() => isShowModal(false)}
      footer={null}
      destroyOnClose={true}
    >
      <ProForm
        onFinish={(values) => handleSubmit(values)}
        request={
          editId == null
            ? () => {
              return {menu_sort: 50, is_menu: 0};
            }
            : () => getData()
        }
        {...formItemLayout}
        layout={formLayout}
      >
        <Form.Item label="菜单父级" name="pidStr">
          <Cascader
            fieldNames={{label: 'menu_name', value: 'admin_menu_id'}}
            options={cateData}
            changeOnSelect
            placeholder="一级菜单"
          />
        </Form.Item>

        <ProFormText
          name="menu_name"
          label="菜单名称"
          placeholder="请输入菜单名称"
          rules={[{required: true, message: '菜单名称不能为空'}]}
        />
        <ProFormText name="menu_path" label="菜单路由" tooltip="菜单路由，需要与页面相对应" placeholder="请输入菜单路由"/>
        <ProFormText name="menu_url" label="菜单链接" tooltip="与后端相对应的链接" placeholder="请输入菜单链接"/>

        <Form.Item name="is_menu" label="是否菜单">
          <Radio.Group>
            <Radio value={0}>否</Radio>
            <Radio value={1}>是</Radio>
          </Radio.Group>
        </Form.Item>

        <ProFormText name="menu_sort" label="菜单排序" placeholder="请输入排序"/>
      </ProForm>
    </Modal>
  );
};

export default Edit;
