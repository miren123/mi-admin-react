import React, { useState, useEffect } from 'react';
import {Modal, Descriptions} from 'antd';
import { getAdminUserLogInfo } from '@/services/rule';

import Field from '@ant-design/pro-field';

const Edit = (props) => {
  const { isModalVisible, isShowModal, logId } = props;
  const [infoData, setInfoData] = useState([]);

  useEffect(() => {
    getData()
  }, [])

  /**
   * 获取数据
   * 作者：糜家智
   * 时间：2021/8/2 18:03
   */
  const getData = () => {
    getAdminUserLogInfo({
      admin_user_log_id: logId
    }).then((res) => {
      res.data.request_param = JSON.stringify(res.data.request_param)
      setInfoData(res.data)
      // setParamData(JSON.stringify(res.data.request_param))
    })
  };

  return (
    <Modal
      title="日志详情"
      visible={isModalVisible}
      onCancel={() => isShowModal(false)}
      footer={null}
      destroyOnClose={true}
      width={680}
    >
      <Descriptions bordered size={'middle'} column={2}>
        <Descriptions.Item label="用户ID">{infoData.admin_user_id}</Descriptions.Item>
        <Descriptions.Item label="用户呢称">{infoData.nickname}</Descriptions.Item>
        <Descriptions.Item label="用户账号" span={2}>{infoData.username}</Descriptions.Item>
        <Descriptions.Item label="菜单ID">{infoData.admin_menu_id}</Descriptions.Item>
        <Descriptions.Item label="菜单名称">{infoData.menu_name}</Descriptions.Item>
        <Descriptions.Item label="菜单链接" span={2}>{infoData.menu_url}</Descriptions.Item>
        <Descriptions.Item label="请求方式">{infoData.request_method}</Descriptions.Item>
        <Descriptions.Item label="请求IP">{infoData.request_ip}</Descriptions.Item>
        <Descriptions.Item label="请求地区">{infoData.request_region}</Descriptions.Item>
        <Descriptions.Item label="请求ISP">{infoData.request_isp}</Descriptions.Item>
        <Descriptions.Item label="请求时间" span={2}>{infoData.create_time}</Descriptions.Item>
        <Descriptions.Item label="请求参数" span={2}>
          <Field
            text={infoData.request_param}
            valueType="jsonCode"
            mode={'read'}
            plain={false}
          />
        </Descriptions.Item>
        <Descriptions.Item label="返回码" span={2}>{infoData.response_code}
        </Descriptions.Item>
        <Descriptions.Item label="返回描述" span={2}>{infoData.response_msg}</Descriptions.Item>
      </Descriptions>

    </Modal>
  );
};

export default Edit;
