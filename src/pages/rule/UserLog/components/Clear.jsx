import React, {useState} from 'react';
import ProForm, {ProFormText} from '@ant-design/pro-form';
import {Modal, message, Form, DatePicker} from 'antd';
import {clearAdminUserLog} from '@/services/rule';

const {RangePicker} = DatePicker;

const Edit = (props) => {
  const {isModalClear, isShowModalClear, actionRef} = props;

  const [formLayout] = useState('horizontal');

  const formItemLayout =
    formLayout === 'horizontal' ? {labelCol: {span: 4}, wrapperCol: {span: 20}} : null;

  /**
   * 作者：糜家智
   * 时间：2022/04/18 17:32:00
   * 功能：提交按钮
   */
  const handleSubmit = async (param) => {
    let res = await clearAdminUserLog(param);
    if (res.code === 200) {
      message.success(res.msg);
      actionRef.current.reload();
      isShowModalClear(false);
    }
  };

  return (
    <Modal
      title="日志清除"
      visible={isModalClear}
      onCancel={() => isShowModalClear(false)}
      footer={null}
      destroyOnClose={true}
    >
      <ProForm
        onFinish={(values) => handleSubmit(values)}
        {...formItemLayout}
        layout={formLayout}
      >
        <ProFormText
          name="admin_user_id"
          label="用户ID"
          placeholder="请输入用户ID"
        />
        <ProFormText
          name="username"
          label="用户账号"
          placeholder="请输入用户账号"
        />
        <ProFormText
          name="admin_menu_id"
          label="菜单ID"
          placeholder="请输入菜单ID"
        />
        <ProFormText name="menu_url" label="菜单链接" placeholder="请输入菜单链接"/>
        <Form.Item
          name="date_range"
          label="日期范围"
        >
          <RangePicker/>
        </Form.Item>
        <div style={{marginLeft: '17%'}}>
          <p>*清除后不可恢复</p>
          <p>*根据填写的条件清除</p>
          <p>*不填写清除条件将清空所有</p>
        </div>
      </ProForm>
    </Modal>
  );
};

export default Edit;
