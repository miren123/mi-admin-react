import React, { useRef, useState } from 'react';
import ProTable from '@ant-design/pro-table';
import { Button, message, Space, Popconfirm } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import {DeleteOutlined } from '@ant-design/icons';
import { getAdminUserLogList, delAdminUserLog } from '@/services/rule';
import Detail from './components/Detail';
import Clear from './components/Clear';

const Index = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalClear, setIsModalClear] = useState(false);
  const [logId, setLogId] = useState(undefined);

  const actionRef = useRef();

  /**
   * 作者：糜家智
   * 时间：2022/04/24 14:45:47
   * 功能：获取数据
   */
  const getData = async (params) => {
    const res = await getAdminUserLogList(params);
    return {
      data: res.data.list,
      success: true,
      total: res.data.count,
    };
  };

  /**
   * 删除列
   * @param id
   * @returns {Promise<void>}
   */
  const handleDel = async (id) => {
    const res = await delAdminUserLog({
      admin_user_log_id: id,
    });
    if (res.code === 200) {
      actionRef.current?.reload();
      message.success(res.msg);
    }
  };

  /**
   * 模态框的显示和隐藏
   */
  const isShowModal = (show, admin_user_log_id = null) => {
    setLogId(admin_user_log_id);
    setIsModalVisible(show);
  };

  const isShowModalClear = (show) => {
    setIsModalClear(show);
  };

  const columns = [
    {
      title: '日志ID',
      dataIndex: 'admin_user_log_id',
      fixed: 'left',
      width: 100,
      align: 'center',
      hideInSearch: true,
    },
    {
      title: '用户账号',
      dataIndex: 'username',
      hideInSearch: true,
    },
    {
      title: '菜单链接',
      dataIndex: 'menu_url',
      hideInSearch: true,
      ellipsis: true
    },
    {
      title: '菜单名称',
      dataIndex: 'menu_name',
    },
    {
      title: '请求方式',
      dataIndex: 'request_method',
      hideInSearch: true,
    },
    {
      title: '请求IP',
      dataIndex: 'request_ip',
    },
    {
      title: '请求地区',
      dataIndex: 'request_region',
      hideInSearch: true,
    },
    {
      title: '请求ISP',
      dataIndex: 'request_isp',
      hideInSearch: true,
    },
    {
      title: '请求时间',
      dataIndex: 'create_time',
      hideInSearch: true,
    },
    {
      title: '返回码',
      dataIndex: 'response_code',
      hideInSearch: true,
    },
    {
      title: '返回描述',
      dataIndex: 'response_msg',
      hideInSearch: true,
      ellipsis: true
    },
    {
      title: '操作',
      fixed: 'right',
      align: 'center',
      width: 120,
      render: (_, record) => (
        <Space size="middle">
          <a onClick={() => isShowModal(true, record.admin_user_log_id)}>详情</a>
          <Popconfirm
            title="确定删除吗？"
            onConfirm={() => {
              handleDel(record.admin_user_log_id);
            }}
            okText="确定"
            cancelText="取消"
          >
            <a href={'javascript'}>删除</a>
          </Popconfirm>
        </Space>
      ),
      hideInSearch: true,
    },
  ];

  return (
    <PageContainer>
      <ProTable
        scroll={{ x: 1000 }}
        columns={columns}
        actionRef={actionRef}
        request={(params) => getData(params)}
        editable={{
          type: 'multiple',
        }}
        rowKey="admin_user_log_id"
        search={{
          labelWidth: 'auto',
        }}
        pagination={{
          pageSize: 10,
        }}
        dateFormatter="string"
        headerTitle="日志列表"
        toolBarRender={() => [
          <Button
            key="button"
            icon={<DeleteOutlined />}
            type="primary"
            onClick={() => isShowModalClear(true)}
          >
            清除日志
          </Button>,
        ]}
      />
      {!isModalVisible ? (
        ''
      ) : (
        <Detail
          isModalVisible={isModalVisible}
          isShowModal={isShowModal}
          logId={logId}
        />
      )}
      {!isModalClear ? (
        ''
      ) : (
        <Clear
          isModalClear={isModalClear}
          actionRef={actionRef}
          isShowModalClear={isShowModalClear}
        />
      )}
    </PageContainer>
  );
};

export default Index;
