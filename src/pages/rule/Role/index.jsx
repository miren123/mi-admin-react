import React, { useRef, useState } from 'react';
import ProTable from '@ant-design/pro-table';
import { Button, Switch, message, Space, Popconfirm } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { PlusOutlined } from '@ant-design/icons';
import { getAdminRoleList, setAdminRoleDisable, delAdminRole } from '@/services/rule';
import Edit from './components/Edit';
import User from './components/User';

const Index = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalUser, setIsModalUser] = useState(false); // 用户模态框显示
  const [editId, setEditId] = useState(undefined);
  const [lockId, setLockId] = useState(0);

  const actionRef = useRef();

  /**
   * 作者：糜家智
   * 时间：2022/04/24 14:45:47
   * 功能：获取数据
   */
  const getData = async (params, sort) => {

    const { current, pageSize, ...paramsData } = params

    // 取出排序的key值
    let sortKey = Object.getOwnPropertyNames(sort)
    let sortData = {
      sort_field: sortKey,
      // 判断是否有key值
      sort_type: sortKey == false ? [] : (sort[sortKey] === 'ascend' ? 'asc' : 'desc')
    }

    const res = await getAdminRoleList({
      page: params.current,
      limit: params.pageSize,
      ...paramsData,
      ...sortData
    });

    return {
      data: res.data.list,
      success: true,
      total: res.data.count,
    };
  };

  /**
   * 用户的启用和禁用
   */
  const handleUserLock = async (id, key) => {
    setLockId(id);
    const setId = key === 0 ? 1 : 0;

    const res = await setAdminRoleDisable({ admin_role_id: id, is_disable: setId });
    if (res.code === 200) {
      message.success(res.msg);
    }
    setLockId(0);
  };

  /**
   * 删除列
   * @param id
   * @returns {Promise<void>}
   */
  const handleDel = async (id) => {
    const res = await delAdminRole({
      admin_role_id: id,
    });
    if (res.code === 200) {
      actionRef.current?.reload();
      message.success(res.msg);
    }
  };

  /**
   * 模态框的显示和隐藏
   */
  const isShowModal = (show, admin_user_id = null, type = 1) => {
    setEditId(admin_user_id);
    // setIsModalVisible(show);

    setEditId(admin_user_id);

    switch (type){
      case 1:
        setIsModalVisible(show);
        break;
      case 2:
        setIsModalUser(show)
        break;
      default:
    }
  };

  const columns = [
    {
      title: '角色ID',
      dataIndex: 'admin_role_id',
      width: 60,
      hideInSearch: true,
    },
    {
      title: '名称',
      dataIndex: 'role_name',
    },
    {
      title: '描述',
      dataIndex: 'role_desc',
    },
    {
      title: '排序',
      dataIndex: 'role_sort',
      sorter: true,
      hideInSearch: true,
    },
    {
      title: '禁用',
      dataIndex: 'is_disable',
      render: (_, record) => (
        <Switch
          checkedChildren="启用"
          unCheckedChildren="禁用"
          defaultChecked={record.is_disable === 1}
          loading={record.admin_role_id === lockId}
          onChange={async () => handleUserLock(record.admin_role_id, record.is_disable)}
        />
      ),
      hideInSearch: true,
    },
    {
      title: '添加时间',
      dataIndex: 'create_time',
      hideInSearch: true,
    },
    {
      title: '修改时间',
      dataIndex: 'update_time',
      hideInSearch: true,
    },
    {
      title: '操作',
      render: (_, record) => (
        <Space size="middle">
          <a onClick={() => isShowModal(true, record.admin_role_id, 2)}>用户</a>
          <a onClick={() => isShowModal(true, record.admin_role_id)}>编辑</a>
          <Popconfirm
            title="确定删除吗？"
            onConfirm={() => {
              handleDel(record.admin_role_id);
            }}
            okText="确定"
            cancelText="取消"
          >
            <a href={'javascript'}>删除</a>
          </Popconfirm>
        </Space>
      ),
      hideInSearch: true,
    },
  ];

  return (
    <PageContainer>
      <ProTable
        columns={columns}
        actionRef={actionRef}
        request={(params, sort) => getData(params, sort)}
        rowKey="admin_role_id"
        search={{
          labelWidth: 'auto',
        }}
        pagination={{
          pageSize: 10,
        }}
        dateFormatter="string"
        headerTitle="角色列表"
        toolBarRender={() => [
          <Button
            key="button"
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => isShowModal(true)}
          >
            新建
          </Button>,
        ]}
      />
      {!isModalVisible ? (
        ''
      ) : (
        <Edit
          isModalVisible={isModalVisible}
          isShowModal={isShowModal}
          actionRef={actionRef}
          editId={editId}
        />
      )}
      {!isModalUser ? (
        ''
      ) : (
        <User
          isModalUser={isModalUser}
          isShowModal={isShowModal}
          editId={editId}
        />
      )}
    </PageContainer>
  );
};

export default Index;
