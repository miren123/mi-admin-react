import React, {useRef, useState} from 'react';
import ProTable from '@ant-design/pro-table';
import {Button, Avatar, Switch, message, Modal, Space, Popconfirm, Image} from 'antd';
import {PageContainer} from '@ant-design/pro-layout';
import {PlusOutlined, UserOutlined} from '@ant-design/icons';
import {getAdminRoleUser, delAdminUser, setAdminUserDisable, setAdminUserSuper, setUserRemove} from '@/services/rule';

const User = (props) => {
  const { isModalUser, isShowModal, editId } = props;

  const [userLockId, setUserLockId] = useState('');

  const actionRef = useRef();

  /**
   * 作者：糜家智
   * 时间：2022/04/24 14:45:47
   * 功能：获取数据
   */
  const getData = async (params) => {

    const res = await getAdminRoleUser( {
      admin_role_id: editId,
      page: params.current,
      limit: params.pageSize,
    });

    return {
      data: res.data.list,
      success: true,
      total: res.data.count,
    };
  };

  /**
   * 用户的启用和禁用
   * @param {*} uid
   */
  const handleUserLock = async (id, key, type) => {
    setUserLockId(id + type);
    const setId = key === 0 ? 1 : 0;

    let res = {};

    switch (type) {
      case 'super':
        res = await setAdminUserSuper({admin_user_id: id, is_super: setId});
        break;
      case 'disable':
        res = await setAdminUserDisable({admin_user_id: id, is_disable: setId});
        break;
      default:
    }

    if (res.code === 200) {
      message.success(res.msg);
    }
    setUserLockId('');
  };

  /**
   * 删除列
   * @param id
   * @returns {Promise<void>}
   */
  const handleRemove = async (id) => {
    const res = await setUserRemove({
      admin_user_id: id,
      admin_role_id: editId
    });
    if (res.code === 200) {
      actionRef.current?.reload();
      message.success(res.msg);
    }
  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      valueType: 'index',
      width: 60,
    },
    {
      title: '账号',
      dataIndex: 'username',
    },
    {
      title: '昵称',
      dataIndex: 'nickname',
      hideInSearch: true,
    },
    {
      title: '邮箱',
      dataIndex: 'email',
    },
    {
      title: '超管',
      dataIndex: 'is_super',
      render: (_, record) => (
        <Switch
          checkedChildren="启用"
          unCheckedChildren="禁用"
          defaultChecked={record.is_super === 1}
          loading={record.admin_user_id + 'super' === userLockId}
          onChange={async () => handleUserLock(record.admin_user_id, record.is_super, 'super')}
        />
      ),
      hideInSearch: true,
    },
    {
      title: '禁用',
      dataIndex: 'is_disable',
      render: (_, record) => (
        <Switch
          checkedChildren="启用"
          unCheckedChildren="禁用"
          defaultChecked={record.is_disable === 1}
          loading={record.admin_user_id + 'disable' === userLockId}
          onChange={async () => handleUserLock(record.admin_user_id, record.is_disable, 'disable')}
        />
      ),
      hideInSearch: true,
    },
    {
      title: '操作',
      align: 'center',
      render: (_, record) => (
        <Space size="middle">
          <Popconfirm
            title="确定要解除该用户与角色的关联吗？"
            onConfirm={() => {
              handleRemove(record.admin_user_id);
            }}
            okText="确定"
            cancelText="取消"
          >
            <a href={'javascript'}>解除</a>
          </Popconfirm>
        </Space>
      ),
      hideInSearch: true,
    },
  ];

  return (
    <Modal
      title={'用户列表'}
      visible={isModalUser}
      onCancel={() => isShowModal(false, null, 2)}
      footer={null}
      destroyOnClose={true}
      width={'50%'}
    >
      <ProTable
        columns={columns}
        actionRef={actionRef}
        request={(params) => getData(params)}
        editable={{
          type: 'multiple',
        }}
        rowKey="admin_user_id"
        search={false}
        pagination={{
          pageSize: 10,
        }}
        dateFormatter="string"
        headerTitle="用户列表"
        toolBarRender={false}
      />
    </Modal>

  );
};

export default User;
