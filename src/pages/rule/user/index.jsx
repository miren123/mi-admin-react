import React, { useRef, useState } from 'react';
import ProTable from '@ant-design/pro-table';
import { Button, Switch, message, Space, Popconfirm } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { PlusOutlined } from '@ant-design/icons';
import { getAdminUserList, delAdminUser, setAdminUserDisable, setAdminUserSuper } from '@/services/rule';
import Edit from './components/Edit';
import Password from './components/Password';

const Index = () => {

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalPassword, setIsModalPassword] = useState(false); // 密码模态框显示
  const [editId, setEditId] = useState(undefined);
  const [userLockId, setUserLockId] = useState('');

  const actionRef = useRef();

  /**
   * 作者：糜家智
   * 时间：2022/04/24 14:45:47
   * 功能：获取数据
   */
  const getData = async (params) => {
    const res = await getAdminUserList(params);
    return {
      data: res.data.list,
      success: true,
      total: res.data.count,
    };
  };

  /**
   * 用户的启用和禁用
   */
  const handleUserLock = async (id, key, type) => {
    setUserLockId(id + type);
    const setId = key === 0 ? 1 : 0;

    let res = {};

    switch (type) {
      case 'super':
        res = await setAdminUserSuper({ admin_user_id: id, is_super: setId });
        break;
      case 'disable':
        res = await setAdminUserDisable({ admin_user_id: id, is_disable: setId });
        break;
      default:
    }

    if (res.code === 200) {
      message.success(res.msg);
    }
    setUserLockId('');
  };

  /**
   * 删除列
   * @param id
   * @returns {Promise<void>}
   */
  const handleDel = async (id) => {
    const res = await delAdminUser({
      admin_user_id: id,
    });
    if (res.code === 200) {
      actionRef.current?.reload();
      message.success(res.msg);
    }
  };

  /**
   * 模态框的显示和隐藏
   */
  const isShowModal = (show, admin_user_id = null, type = 1) => {
    setEditId(admin_user_id);

    switch (type){
      case 1:
        setIsModalVisible(show);
        break;
      case 2:
        setIsModalPassword(show)
        break;
      default:
    }

  };

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      valueType: 'index',
      width: 60,
    },
    {
      title: '账号',
      dataIndex: 'username',
    },
    {
      title: '昵称',
      dataIndex: 'nickname',
    },
    {
      title: '邮箱',
      dataIndex: 'email',
    },
    {
      title: '登录时间',
      dataIndex: 'login_time',
      hideInSearch: true,
    },
    {
      title: '登录次数',
      dataIndex: 'login_num',
      hideInSearch: true,
    },
    {
      title: '排序',
      dataIndex: 'sort',
      hideInSearch: true,
    },
    {
      title: '超管',
      dataIndex: 'is_super',
      render: (_, record) => (
        <Switch
          checkedChildren="启用"
          unCheckedChildren="禁用"
          defaultChecked={record.is_super === 1}
          loading={record.admin_user_id + 'super' === userLockId}
          onChange={async () => handleUserLock(record.admin_user_id, record.is_super, 'super')}
        />
      ),
      hideInSearch: true,
    },
    {
      title: '禁用',
      dataIndex: 'is_disable',
      render: (_, record) => (
        <Switch
          checkedChildren="启用"
          unCheckedChildren="禁用"
          defaultChecked={record.is_disable === 1}
          loading={record.admin_user_id + 'disable' === userLockId}
          onChange={async () => handleUserLock(record.admin_user_id, record.is_disable, 'disable')}
        />
      ),
      hideInSearch: true,
    },
    {
      title: '操作',
      align: 'center',
      render: (_, record) => (
        <Space size="middle">
          <a onClick={() => isShowModal(true, record.admin_user_id, 2)}>密码</a>
          <a onClick={() => isShowModal(true, record.admin_user_id)}>编辑</a>
          <Popconfirm
            title="确定删除吗？"
            onConfirm={() => {
              handleDel(record.admin_user_id);
            }}
            okText="确定"
            cancelText="取消"
          >
            <a href={'javascript'}>删除</a>
          </Popconfirm>
        </Space>
      ),
      hideInSearch: true,
    },
  ];

  return (
    <PageContainer>
      <ProTable
        columns={columns}
        actionRef={actionRef}
        request={(params) => getData(params)}
        editable={{
          type: 'multiple',
        }}
        rowKey="admin_user_id"
        search={{
          labelWidth: 'auto',
        }}
        pagination={{
          pageSize: 10,
        }}
        dateFormatter="string"
        headerTitle="用户列表"
        toolBarRender={() => [
          <Button
            key="button"
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => isShowModal(true)}
          >
            新建
          </Button>,
        ]}
      />
      {!isModalVisible ? (
        ''
      ) : (
        <Edit
          isModalVisible={isModalVisible}
          isShowModal={isShowModal}
          actionRef={actionRef}
          editId={editId}
        />
      )}
      {!isModalPassword ? (
        ''
      ) : (
        <Password
          isModalPassword={isModalPassword}
          isShowModal={isShowModal}
          editId={editId}
        />
      )}
    </PageContainer>
  );
};

export default Index;
