import React, {useEffect, useState} from 'react';
import ProForm, {ProFormText, ProFormTextArea} from '@ant-design/pro-form';
import {Modal, message, Form, Input, Upload, Button, Avatar, Image, Select} from 'antd';
import {getAdminUserInfo, setAdminUserPwd} from '@/services/rule';
import {UploadOutlined, UserOutlined} from '@ant-design/icons';
import styles from './index.less';

const Edit = (props) => {
  const {Option} = Select;

  const {isModalPassword, isShowModal, actionRef, editId} = props;
  const [roleList, setRoleList] = useState([]); // 地区数据
  const [avatar, setAvatar] = useState();
  const [visible, setVisible] = useState(false);

  const [formLayout] = useState('horizontal');

  const formItemLayout = formLayout === 'horizontal' ? {labelCol: {span: 4}, wrapperCol: {span: 20}} : null;

  /**
   * 获取数据
   * 作者：糜家智
   * 时间：2021/8/2 18:03
   */
  const getData = async () => {
    const res = await getAdminUserInfo({
      admin_user_id: editId,
    });
    res.data.password = ''
    return res.data;
  };

  /**
   * 作者：糜家智
   * 时间：2022/04/18 17:32:00
   * 功能：提交按钮
   */
  const handleSubmit = async (values) => {
    const data = values
    data.admin_user_id = editId
    const res = await setAdminUserPwd(data)

    if (res.code === 200) {
      message.success(res.msg)
      isShowModal(false, null, 2);
    }
  };

  /**
   * 作者：糜家智
   * 时间：2022/04/19 16:13:31
   * 功能：头像组件
   */
  function avatarPro() {
    return (
      <Form.Item label="头像">
        <div className={styles.editAvatar}>
          <Avatar
            size={110}
            icon={<UserOutlined/>}
            src={avatar}
            onClick={() => setVisible(true)}
          />

          <Image
            width={200}
            style={{display: 'none'}}
            preview={{
              visible,
              src: avatar,
              onVisibleChange: (value) => {
                setVisible(value);
              },
            }}
          />

          <Upload {...avatarProps}>
            <Button className={styles.btnUpload} icon={<UploadOutlined/>}>
              更换头像
            </Button>
          </Upload>

          <span className={styles.desc}>jpg、png图片，小于100KB，宽高1:1</span>
        </div>
      </Form.Item>
    );
  }

  function handleChange(value) {
    console.log(`selected ${value}`);
  }

  return (
    <Modal
      title={'设置密码'}
      visible={isModalPassword}
      onCancel={() => isShowModal(false, null, 2)}
      footer={null}
      destroyOnClose={true}
    >
      <ProForm
        onFinish={(values) => handleSubmit(values)}
        request={
          editId == null
            ? () => {
              return {sort: 50};
            }
            : () => getData()
        }
        {...formItemLayout}
        layout={formLayout}
      >
        <ProFormText
          name="username"
          label="账号"
          placeholder="请输入账号"
          disabled={true}
        />
        <ProFormText
          name="nickname"
          label="昵称"
          placeholder="请输入昵称"
          disabled={true}
        />
        <ProFormText.Password
          name="password"
          label="密码"
          placeholder="请输入密码"
          rules={[
            {required: true, message: '密码不能为空'},
            {type: 'string', min: 6, message: '密码最小6位'},
          ]}
        />
      </ProForm>
    </Modal>
  );
};

export default Edit;
