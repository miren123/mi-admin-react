import React, {useRef} from 'react';
import ProTable from "@ant-design/pro-table";
import {PageContainer} from '@ant-design/pro-layout';
import { getMemberLog } from '@/services/user';

const Index = () => {

  const actionRef = useRef();

  /**
   * 获取数据
   * 作者：糜家智
   * 时间：2021/8/2 18:03
   */
  const getData = async (params) => {
    const res = await getMemberLog(params)
    return {
      data: res.data.list,
      success: true,
      total: res.data.count
    }
  }

  const columns = [
    {
      title: '日志ID',
      dataIndex: 'log_id',
      fixed: 'left',
      width: '50',
      hideInSearch: true
    },
    {
      title: '会员账号',
      dataIndex: 'username',
    },
    {
      title: '接口链接',
      dataIndex: 'nickname',
      hideInSearch: true
    },
    {
      title: '接口名称',
      dataIndex: 'phone',
    },
    {
      title: '请求方式',
      dataIndex: 'email',
      hideInSearch: true
    },

    {
      title: '请求IP',
      dataIndex: 'login_time',
      hideInSearch: true
    },
    {
      title: '请求地区',
      dataIndex: 'remark',
      hideInSearch: true
    },
    {
      title: '返回码',
      dataIndex: 'sort',
      hideInSearch: true
    },
    {
      title: '返回描述',
      dataIndex: 'sort',
      hideInSearch: true
    },
    {
      title: '请求时间',
      dataIndex: 'sort',
      hideInSearch: true
    },

  ];

  return (
    <PageContainer>
      <ProTable
        columns={columns}
        actionRef={actionRef}
        request={(params) => getData(params)}
        editable={{
          type: 'multiple',
        }}
        rowKey="member_id"
        search={{
          labelWidth: 'auto',
        }}
        pagination={{
          pageSize: 10,
        }}
        dateFormatter="string"
        headerTitle="会员日志"
        toolBarRender={false}
      />
    </PageContainer>
  );
};

export default Index;
