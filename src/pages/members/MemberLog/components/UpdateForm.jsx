import React, {useEffect, useState} from 'react';
import ProForm , { ProFormText } from '@ant-design/pro-form';
import { Modal, message } from 'antd';
import { editUser } from '@/services/user';


const CreateForm = (props) => {
  const { isModalVisible, isShowModal, actionRef, editData } = props;

  const [formLayout] = useState('horizontal');

  const formItemLayout =
    formLayout === 'horizontal'
      ? {
        labelCol: { span: 4 },
        wrapperCol: { span: 20 },
      }
      : null;

  /**
   * 修改用户
   * @param {*} values
   */
  const updateUser = async values => {
    const data = values
    data.member_id = editData.member_id
    const res = await editUser(data)
    if(res.code === 200){
      message.success(res.msg)
      actionRef.current.reload()
      isShowModal(false)
    }
  }


  return (
    <Modal
      title="编辑用户"
      visible={isModalVisible}
      onCancel={() => isShowModal(false)}
      footer={null}
      destroyOnClose={true}
    >
      <ProForm
        onFinish={ values => updateUser(values) }
        initialValues={editData}
        {...formItemLayout}
        layout={formLayout}
      >
        <ProFormText
          name="username"
          label="账号"
          tooltip="账号用英文和字母"
          placeholder="请输入账号"
          rules={[
            {required: true, message: '账号不能为空'}
          ]}
        />
        <ProFormText
          name="nickname"
          label="昵称"
          placeholder="请输入昵称"
          rules={[
            {required: true, message: '昵称不能为空'}
          ]}
        />
        <ProFormText.Password
          name="password"
          label="密码"
          placeholder="请输入密码"
          // rules={[
          //   {required: true, message: '密码不能为空'},
          //   {type: 'string', min: 6, message: '密码最小6位'}
          // ]}
        />
        <ProFormText
          name="phone"
          label="手机"
          placeholder="请输入手机"
          rules={[
            {required: true, message: '手机不能为空'}
          ]}
        />
        <ProFormText
          name="email"
          label="邮箱"
          placeholder="请输入邮箱"
          rules={[
            {required: true, message: '邮箱不能为空'},
            {type: 'email', message: '邮箱格式不对'}
          ]}
        />
        <ProFormText
          name="region_id"
          label="地区"
          placeholder="请选择地区"
        />
        <ProFormText
          name="remark"
          label="备注"
          placeholder="请输入备注"
        />
        <ProFormText
          name="sort"
          label="排序"
          placeholder="请输入排序"
        />
      </ProForm>

    </Modal>
  );
};

export default CreateForm;
