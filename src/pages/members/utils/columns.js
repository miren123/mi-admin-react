const columnsData = []

columnsData.memberColumns = [
  // {
  //   title: '会员ID',
  //   dataIndex: 'member_id',
  //   fixed: 'left',
  //   width: '50'
  // },
  {
    title: '序号',
    dataIndex: 'index',
    valueType: 'index',
    width: 60,
  },
  {
    title: '头像',
    dataIndex: 'avatar',
    render: (_, record) => <Avatar size={32} src={record.avatar} icon={<UserOutlined/>}/>,
    hideInSearch: true
  },
  {
    title: '账号',
    dataIndex: 'username',
  },
  {
    title: '昵称',
    dataIndex: 'nickname',
    hideInSearch: true
  },
  {
    title: '手机',
    dataIndex: 'phone',
  },
  {
    title: '邮箱',
    dataIndex: 'email',
  },

  {
    title: '登录时间',
    dataIndex: 'login_time',
    hideInSearch: true
  },
  {
    title: '备注',
    dataIndex: 'remark',
    hideInSearch: true
  },
  {
    title: '排序',
    dataIndex: 'sort',
    hideInSearch: true
  },
  {
    title: '禁用',
    dataIndex: 'is_disable',
    render: (_, record) => <Switch
      checkedChildren="启用"
      unCheckedChildren="禁用"
      defaultChecked={record.is_disable === 0}
      onChange={async () => handleUserLock(record.member_id)}
    />,
    hideInSearch: true
  },
  {
    title: '操作',
    render: (_, record) => <a onClick={() => isShowModal(true, record)}>编辑</a>,
    hideInSearch: true
  },

];

export default columnsData