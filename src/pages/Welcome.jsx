import React from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card, Col, Row } from 'antd';
import  './Welcome.less'
import { login } from '@/services/ant-design-pro/api';

export default () => {

  const { Meta } = Card;

  return (
    <PageContainer>
      <div className="site-card-wrapper">
        <Row gutter={16}>
          <Col span={4}>
            <Card
              hoverable
              style={{ width: 240 }}
              cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
            >
              <Meta title="Europe Street beat" description="www.instagram.com" />
            </Card>
          </Col>
        </Row>
      </div>
    </PageContainer>
  );
};
