import React, {useRef} from 'react';
import ProTable from '@ant-design/pro-table';
import {message, Space, Avatar, Modal, Table} from 'antd';
import {FundViewOutlined, ExclamationCircleOutlined} from '@ant-design/icons';
import {recoverList, recoverReco, recoverDel} from '@/services/cms';

const {confirm} = Modal;

const Index = (props) => {
  const {isModalRecycle, isShowRecycleModal, actionRefIndex} = props;

  const actionRef = useRef();

  /**
   * 获取数据
   * 作者：糜家智
   * 时间：2021/8/2 18:03
   */
  const getData = async (params) => {
    const res = await recoverList(params);
    return {
      data: res.data.list,
      success: true,
      total: res.data.count,
    };
  };

  /**
   * 删除列
   * @param id
   * @returns {Promise<void>}
   */
  const handleOk = async (type = 1, data = [], onCleanSelected = undefined) => {
    confirm({
      icon: <ExclamationCircleOutlined/>,
      content: type === 1 ? '确定要彻底删除这一条内容吗？' : '确定要彻底删除已选择的内容吗？',
      async onOk() {
        let param = [];

        type === 1 ? param[0] = data : param = data
        const res = await recoverDel({
          content: param
        });

        if (res.code === 200) {
          actionRef.current?.reload();
          onCleanSelected ? onCleanSelected() : ''
          message.success(res.msg);
        }
      },
      onCancel() {
      },
    });
  };

  const onRecoverReco = (type = 1, data = [], onCleanSelected = undefined) => {
    confirm({
      icon: <ExclamationCircleOutlined/>,
      content: type === 1 ? '确定要恢复这一条内容吗？' : '确定要恢复已选择的内容吗？',
      async onOk() {
        let param = []
        type === 1 ? param[0] = data : param = data
        const res = await recoverReco({content: param})
        if (res.code === 200){
          message.success(res.msg)
          onCleanSelected ? onCleanSelected() : ''
          actionRef.current?.reload()
          actionRefIndex.current?.reload()
        }
      },
      onCancel() {
      },
    });
  }

  const columns = [
    {
      title: 'ID',
      dataIndex: 'content_id',
      key: 'content_id',
      hideInSearch: true,
    },
    {
      title: '图片',
      dataIndex: 'img_url',
      key: 'img_url',
      align: 'center',
      render: (_, record) => <Avatar size={32} src={record.img_url} icon={<FundViewOutlined/>}/>,
      hideInSearch: true,
    },
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
      align: 'center',
    },
    {
      title: '分类名称',
      dataIndex: 'category_name',
      key: 'category_name',
      hideInSearch: true,
    },
    {
      title: '排序',
      dataIndex: 'sort',
      key: 'sort',
      hideInSearch: true,
    },
    {
      title: '添加时间',
      dataIndex: 'create_time',
      key: 'create_time',
      hideInSearch: true,
    },
    {
      title: '修改时间',
      dataIndex: 'update_time',
      key: 'update_time',
      hideInSearch: true,
    },
    {
      title: '操作',
      render: (_, record) => (
        <Space size="middle">
          <a onClick={() => onRecoverReco(1, record)}>恢复</a>
          <a onClick={() => handleOk(1, record)}>删除</a>
        </Space>
      ),
      hideInSearch: true,
    },
  ];

  return (
    <Modal
      title={'回收站'}
      visible={isModalRecycle}
      onCancel={() => isShowRecycleModal(false)}
      footer={false}
      destroyOnClose={true}
      width={1000}
    >
      <ProTable
        columns={columns}
        actionRef={actionRef}
        request={(params) => getData(params)}
        // dataSource={data}
        editable={{
          type: 'multiple',
        }}
        rowKey="content_id"
        dateFormatter="string"
        headerTitle="内容管理列表"
        rowSelection={{
          // 自定义选择项参考: https://ant.design/components/table-cn/#components-table-demo-row-selection-custom
          // 注释该行则默认不显示下拉选项
          // selections: [Table.SELECTION_ALL, Table.SELECTION_INVERT],
        }}
        tableAlertRender={({selectedRowKeys, selectedRows, onCleanSelected}) => {
          return (
            <Space size={24}>
              <span>
                已选 {selectedRowKeys.length} 项
                <a style={{marginLeft: 8}} onClick={onCleanSelected}>
                  取消选择
                </a>
              </span>
            </Space>
          )
        }}
        tableAlertOptionRender={({selectedRowKeys, selectedRows, onCleanSelected}) => {
          return (
            <Space size={16}>
              {/*<a onClick={() => {onRecoverReco(2, selectedRows);onCleanSelected();}}>批量恢复</a>*/}
              <a onClick={() => {onRecoverReco(2, selectedRows, onCleanSelected)}}>批量恢复</a>
              <a onClick={() => {handleOk(2, selectedRows, onCleanSelected)}}>批量删除</a>
            </Space>
          );
        }}
        toolBarRender={false}
      />
    </Modal>
  );
};

export default Index;
