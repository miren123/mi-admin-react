import React, { useRef, useState } from 'react';
import ProTable from '@ant-design/pro-table';
import { Button, Switch, message, Space, Popconfirm } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { PlusOutlined } from '@ant-design/icons';
import { getCategoryList, isHide, delCate } from '@/services/cms';
import Edit from './components/Edit';

const Index = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [editId, setEditId] = useState(undefined);
  const [cateData, setCateData] = useState([]);
  const [lockId, setLockId] = useState(0);

  const actionRef = useRef();

  /**
   * 作者：糜家智
   * 时间：2022/04/21 15:58:03
   * 功能：获取数据
   */
  const getData = async () => {
    const res = await getCategoryList();
    setCateData(res.data.list);
    return {
      data: res.data.list,
      success: true,
      total: res.data.count,
    };
  };

  /**
   * 作者：糜家智
   * 时间：2022/04/21 15:57:52
   * 功能：用户的启用和禁用
   */
  const handleCategoryLock = async (category_id, hide) => {
    setLockId(category_id)
    const res = await isHide({ category_ids: category_id + '', is_hide: hide === 1 ? 0 : 1 });
    if (res.code === 200) {
      message.success(res.msg);
    }
    setLockId(0)
  };

  /**
   * 作者：糜家智
   * 时间：2022/04/21 15:57:31
   * 功能：模态框的显示和隐藏
   */
  const isShowModal = (show, category_id = null) => {
    setEditId(category_id);
    setIsModalVisible(show);
  };

  /**
   * 作者：糜家智
   * 时间：2022/04/21 15:57:21
   * 功能：删除列
   */
  const handleDel = async (category_id) => {
    const res = await delCate({ category_ids: category_id + '' });
    if (res.code === 200) {
      actionRef.current?.reload();
      message.success(res.msg);
    }
  };

  const columns = [
    {
      title: '分类名称',
      dataIndex: 'category_name',
      key: 'category_name',
    },
    {
      title: '分类ID',
      dataIndex: 'category_id',
      key: 'category_id',
    },
    {
      title: '父级ID',
      dataIndex: 'category_pid',
      key: 'category_pid',
    },
    {
      title: '排序',
      dataIndex: 'sort',
      key: 'sort',
    },
    {
      title: '是否隐藏',
      dataIndex: 'is_hide',
      render: (_, record) => (
        <Switch
          checkedChildren="启用"
          unCheckedChildren="禁用"
          defaultChecked={record.is_hide === 1}
          loading={record.category_id === lockId}
          onChange={async () => handleCategoryLock(record.category_id, record.is_hide)}
        />
      ),
      hideInSearch: true,
    },
    {
      title: '添加时间',
      dataIndex: 'create_time',
      key: 'create_time',
    },
    {
      title: '修改时间',
      dataIndex: 'update_time',
      key: 'update_time',
    },
    {
      title: '操作',
      render: (_, record) => (
        <Space size="middle">
          <a onClick={() => isShowModal(true, record.category_id)}>编辑</a>
          <Popconfirm
            title="确定删除么？"
            onConfirm={() => handleDel(record.category_id)}
            okText="确定"
            cancelText="取消"
          >
            <a href={'javascript'}>删除</a>
          </Popconfirm>
        </Space>
      ),
      hideInSearch: true,
    },
  ];

  return (
    <PageContainer>
      <ProTable
        columns={columns}
        actionRef={actionRef}
        request={() => getData()}
        editable={{
          type: 'multiple',
        }}
        rowKey="category_id"
        search={false}
        pagination={false}
        dateFormatter="string"
        headerTitle="内容分类列表"
        toolBarRender={() => [
          <Button
            key="button"
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => isShowModal(true)}
          >
            新建
          </Button>,
        ]}
      />
      {!isModalVisible ? (
        ''
      ) : (
        <Edit
          isModalVisible={isModalVisible}
          isShowModal={isShowModal}
          actionRef={actionRef}
          editId={editId}
          cateData={cateData}
        />
      )}
    </PageContainer>
  );
};

export default Index;
