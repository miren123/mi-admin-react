import React, {useRef, useState} from 'react';
import { PageContainer, GridContent } from '@ant-design/pro-layout';
import ProForm, {ProFormText, ProFormTextArea} from '@ant-design/pro-form';
import { message, Form, Upload, Image, Button, Card, Col, Row, Avatar} from 'antd';
import { getSettingInfo, editSetting } from "@/services/cms";
import { UploadOutlined, UserOutlined } from '@ant-design/icons';

import styles from "./index.less";

const Index = () => {
  const [subLoading, setSubLoading] = useState(false)
  const [logoUrl, setLogoUrl] = useState('');
  const [logoPath, setLogoPath] = useState('');
  const [visible, setVisible] = useState(false);

  const actionRef = useRef();

  const [formLayout] = useState('horizontal');

  const formItemLayout = formLayout === 'horizontal' ? { labelCol: {span: 2}, wrapperCol: {span: 13},} : null;

  const handleSubmit = async (values) => {
    setSubLoading(true)
    const params = values;
    params.logo = logoPath
    params.logo_url = logoUrl

    const res = await editSetting(params)

    if (res.code === 200) {
      setSubLoading(false)
      message.success(res.msg);
    }
  };

  const getData = async () => {
    const res = await getSettingInfo()
    // logoUrl.path = res.data.logo
    // let data = []
    // data['url'] = res.data.logo_url
    // setLogoUrl(logoUrl => [...logoUrl, data])
    // console.log(logoUrl)
    setLogoUrl(res.data.logo_url)
    return res.data
  };

  const avatarProps = {
    name: 'file',
    action: '/admin/cms.Setting/upload',
    headers: {
      AdminToken: localStorage.getItem('access_token') || '',
    },
    accept: '.jpg,.png,.jpeg',
    showUploadList: false,
    beforeUpload(file) {
      if (file.size > 102400) {
        message.warning('图片不可大于100k~');
        return false;
      }
    },
    onChange(info) {
      if (info.file.status === 'done') {
        let res = info.file.response;
        if (res.code === 200) {
          message.success('LOGO上传成功~');
          setLogoUrl(res.data.url);
          setLogoPath(res.data.path);
        } else {
          message.error(res.msg);
        }
      } else if (info.file.status === 'error') {
        message.error('LOGO上传失败！');
      }
    },
  };

  return (
    <PageContainer>
      <GridContent >
        <Row gutter={24}>
          <Col lg={24} md={24}>
            <Card
              bordered={false}
              style={{
                marginBottom: 24,
              }}
            >
              <ProForm
                formRef={actionRef}
                onFinish={(values) => handleSubmit(values)}
                request={() => getData()}
                {...formItemLayout}
                layout={formLayout}
                submitter={false}
              >

                <Form.Item label="LOGO">
                  <div className={styles.editAvatar}>
                    <Avatar
                      size={110}
                      icon={<UserOutlined />}
                      src={logoUrl}
                      onClick={() => setVisible(true)}
                    />

                    <Image
                      width={200}
                      style={{ display: 'none' }}
                      preview={{
                        visible,
                        src: logoUrl,
                        onVisibleChange: (value) => {
                          setVisible(value);
                        },
                      }}
                    />

                    <Upload {...avatarProps}>
                      <Button className={styles.btnUpload} icon={<UploadOutlined />}>
                        单击上传
                      </Button>
                    </Upload>

                    {/*<span className={styles.desc}>jpg、png图片，小于100KB，宽高1:1</span>*/}
                  </div>
                </Form.Item>

                <ProFormText
                  name="name"
                  label="名称"
                  placeholder="请输入名称"
                  rules={[{required: true, message: '名称不能为空'}]}
                />
                <ProFormText name="title" label="标题" placeholder="请输入标题"/>
                <ProFormText name="keywords" label="关键词" placeholder="请输入关键词"/>
                <ProFormTextArea name="description" label="描述" placeholder="请输入描述"/>
                <ProFormText name="icp" label="备案号" placeholder="请输入备案号"/>
                <ProFormText name="copyright" label="版权" placeholder="请输入版权"/>
                <ProFormText name="address" label="地址" placeholder="请输入地址"/>
                <ProFormText name="tel" label="电话" placeholder="请输入电话"/>
                <ProFormText name="email" label="邮箱" placeholder="请输入电话"/>
                <ProFormText name="qq" label="QQ" placeholder="请输入QQ"/>
                <ProFormText name="wechat" label="微信" placeholder="请输入微信"/>

                <Form.Item wrapperCol={{ offset: 2 }}>

                  <Button type="primary" htmlType="submit" loading={subLoading}>
                    提交
                  </Button>

                </Form.Item>

              </ProForm>

            </Card>
          </Col>
        </Row>

      </GridContent>
    </PageContainer>
  );
};

export default Index;
