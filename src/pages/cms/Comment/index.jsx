import React, {useRef} from 'react';
import ProTable from "@ant-design/pro-table";
import {PageContainer} from '@ant-design/pro-layout';
import {getCommentList} from '@/services/cms';

const Index = () => {

  const actionRef = useRef();

  /**
   * 获取数据
   * 作者：糜家智
   * 时间：2021/8/2 18:03
   */
  const getData = async (params) => {
    const res = await getCommentList(params)
    return {
      data: res.data.list,
      success: true,
      total: res.data.count
    }
  }

  const columns = [
    {
      title: 'ID',
      dataIndex: 'member_id',
      fixed: 'left',
      width: '50',
      hideInSearch: true
    },
    {
      title: '称呼',
      dataIndex: 'username',
      hideInSearch: true
    },
    {
      title: '昵称',
      dataIndex: 'nickname',
      hideInSearch: true
    },
    {
      title: '手机',
      dataIndex: 'phone',
    },
    {
      title: '标题',
      dataIndex: 'email',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      hideInSearch: true
    },
    {
      title: '未读',
      dataIndex: 'remark',
      hideInSearch: true
    },
  ];

  return (
    <PageContainer>
      <ProTable
        columns={columns}
        actionRef={actionRef}
        request={(params) => getData(params)}
        editable={{
          type: 'multiple',
        }}
        rowKey="member_id"
        search={{
          labelWidth: 'auto',
        }}
        pagination={{
          pageSize: 10,
        }}
        dateFormatter="string"
        headerTitle="留言管理"
        toolBarRender={false}
      />
    </PageContainer>
  );
};

export default Index;
