import React from 'react';
import { HomeOutlined, ContactsOutlined, ClusterOutlined } from '@ant-design/icons';
import {GridContent } from '@ant-design/pro-layout';
import { Card, Col, Row, Divider, Tag } from 'antd';
import styles from './Center.less';
import { useModel } from 'umi';

import Projects from './components/Projects';

const TagList = () => {
  const tags = [
    {
      label: '吴彦祖',
      color: 'magenta',
    },
    {
      label: '来自星星的你~',
      color: 'red',
    },
    {
      label: '蓝色',
      color: 'volcano',
    },
    {
      label: '程序百科',
      color: 'orange',
    },
    {
      label: '巧克力',
      color: 'gold',
    },
    {
      label: '你的名字',
      color: 'lime',
    },
    {
      label: '故蓝寻',
      color: 'green',
    },
    {
      label: 'react',
      color: 'cyan',
    },
    {
      label: '微信小程序',
      color: 'blue',
    },
    {
      label: '彼岸境界',
      color: 'geekblue',
    },
    {
      label: '海纳百川',
      color: 'purple',
    },
  ];

  return (
    <div className={styles.tags}>
      <div className={styles.tagsTitle}>标签</div>
      {tags.map((item, index) => (
        <Tag key={index} color={item.color}>
          {item.label}
        </Tag>
      ))}
    </div>
  );
};

const Center = () => {
  const { initialState, setInitialState } = useModel('@@initialState');
  const { currentUser } = initialState;

  const loading = false;

  const renderUserInfo = () => {
    return (
      <div className={styles.detail}>
        <p>
          <ContactsOutlined
            style={{
              marginRight: 8,
            }}
          />
          PHP全栈工程师
        </p>
        <p>
          <ClusterOutlined
            style={{
              marginRight: 8,
            }}
          />
          励志要成为火影的男人
        </p>
        <p>
          <HomeOutlined
            style={{
              marginRight: 8,
            }}
          />
          来自木叶村
        </p>
      </div>
    );
  };

  return (
    <GridContent>
      <Row gutter={24}>
        <Col lg={7} md={24}>
          <Card
            bordered={false}
            style={{
              marginBottom: 24,
            }}
            loading={loading}
          >
            {!loading && currentUser && (
              <>
                <div>
                  <div className={styles.avatarHolder}>
                    <img alt="" src={currentUser.avatar} />
                    <div className={styles.name}>{currentUser.nickname}</div>
                    <div>{currentUser.desc}</div>
                  </div>
                </div>
                {renderUserInfo()}
                <Divider dashed />
                <TagList />
              </>
            )}
          </Card>
        </Col>
        <Col lg={17} md={24}>
          <Card
            className={styles.tabsCard}
            bordered={false}
          >
            <Projects />
          </Card>
        </Col>
      </Row>
    </GridContent>
  );
};

export default Center;
