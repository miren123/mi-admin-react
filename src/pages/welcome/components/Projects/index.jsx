import {Card, Image, List} from 'antd';
import React, { useState, useEffect } from 'react';

import { projectList } from '@/services/user';

import styles from './index.less';

const Projects = () => {
  const [listData, setListData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [visible, setVisible] = useState(false);
  const [codeImg, setCodeImg] = useState();


  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    projectList().then((res) =>{
      setListData(res.data);
      setLoading(false);
    })
  };

  const openImg = (img) =>{
    setCodeImg(img)
    setVisible(true)
  }

  return (
    <List
      className={styles.coverCardList}
      rowKey="id"
      grid={{
        gutter: 24,
        xxl: 3,
        xl: 3,
        lg: 2,
        md: 2,
        sm: 2,
        xs: 2,
      }}
      loading={loading}
      dataSource={listData || []}
      renderItem={(item) => (
        <List.Item>
          <Card
            className={styles.card}
            hoverable
            cover={<img className={styles.coverImg} alt={item.title} src={item.img} />}
          >
            <Card.Meta title={<a>{item.title}</a>} description={item.desc} />
            <div className={styles.cardItemContent}>
              {item.download == true ? (
                <a target="_blank" className={styles.button} href={item.link} rel="noreferrer">
                  下载
                </a>
              ) : item.code == true ? (
                <a className={styles.button} onClick={() => openImg(item.img)}>
                  扫一扫
                </a>
              ) : (
                <a target="_blank" className={styles.button} href={item.link} rel="noreferrer">
                  查看
                </a>
              )}
            </div>
          </Card>

          <Image
            width={0}
            style={{display: 'none'}}
            preview={{
              visible,
              src: codeImg,
              onVisibleChange: (value) => {
                setVisible(value);
              },
            }}
          />

        </List.Item>
      )}
    />
  );
};

export default Projects;
