import React, { useEffect, useState } from 'react';
import {PageContainer} from '@ant-design/pro-layout';
import { Button } from 'antd';

const Index = () => {
  const [height, setHeight] = useState(600)

  useEffect(() =>{
    let h = screenHeight(200)
    setHeight(h)
  }, [])


  function screenHeight(height = 230) {
    let defaultHeight = 880
    let clientHeight = document.documentElement.clientHeight || document.body.clientHeight
    if (clientHeight) {
      return clientHeight - height
    } else {
      return defaultHeight - height
    }
  }

  return (
    <PageContainer
      header={{
        title: '接口文档',
        breadcrumb: {},
      }}
      extra={[
        <Button type="primary" key={1}>
          <a href={'http://mi.admin.com/apidoc/'} target={'_blank'} rel="noreferrer">新标签打开</a>
        </Button>,
      ]}
    >
      <iframe src="http://mi.admin.com/apidoc/" width={'100%'} height={height}/>
    </PageContainer>
  );
};

export default Index;
