/* eslint-disable */
import { request } from 'umi';
/** 获取当前的用户 GET /api/currentUser */

/**
 * 作者：糜家智
 * 时间：2022/04/28 16:53:25
 * 功能：获取用户信息
 */
export async function currentUser(options) {
  return request('/admin/AdminUserCenter/info', {
    method: 'GET',
    ...(options || {}),
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/28 16:54:00
 * 功能：获取可用菜单
 */
export async function getAdminUserMenu(options) {
  return request('/admin/AdminUserCenter/menu', {
    method: 'GET',
    ...(options || {}),
  });
}

export async function outLogin(options) {
  return request('/admin/AdminLogin/logout', {
    method: 'POST',
    ...(options || {}),
  });
}
/** 登录接口 POST /api/login/account */

export async function login(body, options) {
  return request('/admin/AdminLogin/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

export async function editAdminUser(body, options) {
  return request('/admin/AdminUserCenter/edit', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/**
 * 作者：糜家智
 * 时间：2022/5/13
 * 功能：修改密码
 */
export async function editAdminUserPwd(body, options) {
  return request('/admin/AdminUserCenter/pwd', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
