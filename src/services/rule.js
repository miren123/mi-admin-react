import { request } from 'umi';

/**
 * 作者：糜家智
 * 时间：2022/04/22 19:22:45
 * 功能：菜单列表
 */
export async function getAdminMenuList(params, options) {
  return request('/admin/AdminMenu/list', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/22 19:51:00
 * 功能：是否菜单
 */
export async function setIsMenu(data) {
  return request('/admin/AdminMenu/menu', {
    method: 'POST',
    data,
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/22 19:51:00
 * 功能：是否禁用
 */
export async function setMenuDisable(data) {
  return request('/admin/AdminMenu/disable', {
    method: 'POST',
    data,
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/22 19:52:00
 * 功能：无需权限
 */
export async function setMenuUnauth(data) {
  return request('/admin/AdminMenu/unauth', {
    method: 'POST',
    data,
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/22 19:52:17
 * 功能：无需登录
 */
export async function setMenuUnlogin(data) {
  return request('/admin/AdminMenu/unlogin', {
    method: 'POST',
    data,
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/23 17:21:39
 * 功能：菜单详情
 */
export async function getMenuInfo(params, options) {
  return request('/admin/AdminMenu/info', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/23 21:59:07
 * 功能：添加菜单
 */
export async function addMenu(data) {
  return request('/admin/AdminMenu/add', {
    method: 'POST',
    data,
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/23 21:59:07
 * 功能：编辑菜单
 */
export async function editMenu(data) {
  return request('/admin/AdminMenu/edit', {
    method: 'POST',
    data,
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/23 21:59:07
 * 功能：编辑菜单
 */
export async function delMenu(params, options) {
  return request('/admin/AdminMenu/dele', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/24 14:43:19
 * 功能：获取用户列表（管理员）
 */
export async function getAdminUserList(params, options) {
  return request('/admin/AdminUser/list', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/27 18:59:01
 * 功能：设置超管
 */
export async function setAdminUserSuper(data) {
  return request('/admin/AdminUser/super', {
    method: 'POST',
    data,
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/27 18:26:47
 * 功能：设置密码
 */
export async function setAdminUserPwd(data) {
  return request('/admin/AdminUser/pwd', {
    method: 'POST',
    data
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/24 14:43:35
 * 功能：添加用户（管理员）
 */
export async function addAdminUser(data) {
  return request('/admin/AdminUser/add', {
    method: 'POST',
    data
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/24 14:43:49
 * 功能：编辑用户（管理员）
 */
export async function editAdminUser(data) {
  return request('/admin/AdminUser/edit', {
    method: 'POST',
    data
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/24 14:44:10
 * 功能：删除用户（管理员）
 */
export async function delAdminUser(params, options) {
  return request('/admin/AdminUser/dele', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/24 15:56:30
 * 功能：用户详情
 */
export async function getAdminUserInfo(params, options) {
  return request('/admin/AdminUser/info', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/24 15:56:51
 * 功能：
 */
export async function setAdminUserDisable(data) {
  return request('/admin/AdminUser/disable', {
    method: 'POST',
    data
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/24 15:56:51
 * 功能：日志列表
 */
export async function getAdminUserLogList(params, options) {
  return request('/admin/AdminUserLog/list', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/24 16:49:51
 * 功能：删除日志
 */
export async function delAdminUserLog(data) {
  return request('/admin/AdminUserLog/dele', {
    method: 'POST',
    data
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/24 17:47:10
 * 功能：日志详情
 */
export async function getAdminUserLogInfo(params, options) {
  return request('/admin/AdminUserLog/info', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/24 20:32:48
 * 功能：清除日志
 */
export async function clearAdminUserLog(data) {
  return request('/admin/AdminUserLog/clear', {
    method: 'POST',
    data
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/27 10:11:28
 * 功能：角色列表
 */
export async function getAdminRoleList(params, options) {
  return request('/admin/AdminRole/list', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/27 10:16:37
 * 功能：角色禁用
 */
export async function setAdminRoleDisable(data) {
  return request('/admin/AdminRole/disable', {
    method: 'POST',
    data
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/27 10:17:46
 * 功能：删除角色
 */
export async function delAdminRole(data) {
  return request('/admin/AdminRole/dele', {
    method: 'POST',
    data
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/27 14:59:27
 * 功能：角色详情
 */
export async function getAdminRoleInfo(params, options) {
  return request('/admin/AdminRole/info', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/27 15:50:56
 * 功能：添加角色
 */
export async function addAdminRole(data) {
  return request('/admin/AdminRole/add', {
    method: 'POST',
    data
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/27 15:51:02
 * 功能：编辑角色
 */
export async function editAdminRole(data) {
  return request('/admin/AdminRole/edit', {
    method: 'POST',
    data
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/28 14:34:48
 * 功能：解除角色
 */
export async function setUserRemove(data) {
  return request('/admin/AdminRole/userRemove', {
    method: 'POST',
    data
  });
}

/**
 * 作者：糜家智
 * 时间：2022/04/28 14:36:01
 * 功能：获取绑定角色的用户
 */
export async function getAdminRoleUser(params, options) {
  return request('/admin/AdminRole/user', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}
