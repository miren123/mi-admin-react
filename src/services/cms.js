/* eslint-disable */
import { request } from 'umi';

/**
 * 获取内容分类列表
 * 作者：糜家智
 * 时间：2021/7/26 18:07
 */
export async function getCategoryList(params, options) {
  return request('/admin/cms.Category/list', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 内容分类显示和隐藏
 * 作者：糜家智
 * 时间：2021/7/26 18:07
 */
export async function isHide(data) {
  return request('/admin/cms.Category/ishide', {
    method: 'POST',
    data,
  });
}

/**
 * 添加分类
 * 作者：糜家智
 * 时间：2021/8/10 15:18
 */
export async function addCate(data) {
  return request('/admin/cms.Category/add', {
    method: 'POST',
    data,
  });
}

/**
 * 编辑分类
 * 作者：糜家智
 * 时间：2021/8/10 15:18
 */
export async function editCate(data) {
  return request('/admin/cms.Category/edit', {
    method: 'POST',
    data,
  });
}

/**
 * 编辑分类
 * 作者：糜家智
 * 时间：2021/8/10 15:18
 */
export async function infoCate(params, options) {
  return request('/admin/cms.Category/info', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 删除分类
 * 作者：糜家智
 * 时间：2021/8/10 15:18
 */
export async function delCate(data) {
  return request('/admin/cms.Category/dele', {
    method: 'POST',
    data,
  });
}

/**
 * 内容管理列表
 * 作者：糜家智
 * 时间：2021/8/10 15:18
 */
export async function contentList(params, options) {
  return request('/admin/cms.Content/list', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 置顶
 * 作者：糜家智
 * 时间：2021/8/10 15:18
 */
export async function isTop(data) {
  return request('/admin/cms.Content/istop', {
    method: 'POST',
    data,
  });
}
/**
 * 热门
 * 作者：糜家智
 * 时间：2021/8/10 15:18
 */
export async function isHot(data) {
  return request('/admin/cms.Content/ishot', {
    method: 'POST',
    data,
  });
}
/**
 * 推荐
 * 作者：糜家智
 * 时间：2021/8/10 15:18
 */
export async function isRec(data) {
  return request('/admin/cms.Content/isrec', {
    method: 'POST',
    data,
  });
}

/**
 * 隐藏
 * 作者：糜家智
 * 时间：2021/8/10 15:18
 */
export async function isHideContent(data) {
  return request('/admin/cms.Content/ishide', {
    method: 'POST',
    data,
  });
}

/**
 * 添加内容
 * 作者：糜家智
 * 时间：2021/8/12 18:09
 */
export async function addContent(data) {
  return request('/admin/cms.Content/add', {
    method: 'POST',
    data,
  });
}

/**
 * 编辑内容
 * 作者：糜家智
 * 时间：2021/8/12 18:09
 */
export async function editContent(data) {
  return request('/admin/cms.Content/edit', {
    method: 'POST',
    data,
  });
}

/**
 * 删除内容
 * 作者：糜家智
 * 时间：2021/8/12 18:09
 */
export async function delContent(data) {
  return request('/admin/cms.Content/dele', {
    method: 'POST',
    data,
  });
}

/**
 * 删除内容
 * 作者：糜家智
 * 时间：2021/8/12 18:09
 */
export async function infoContent(params) {
  return request('/admin/cms.Content/info', {
    method: 'GET',
    params: { ...params },
  });
}

/**
 * 删除内容
 * 作者：糜家智
 * 时间：2021/8/12 18:09
 */
export async function getCommentList(params) {
  return request('/admin/cms.Comment/list', {
    method: 'GET',
    params: { ...params },
  });
}

/**
 * 内容设置详情
 * 作者：糜家智
 * 时间：2021/8/12 18:09
 */
export async function getSettingInfo(params) {
  return request('/admin/cms.Setting/info', {
    method: 'GET',
    params: { ...params },
  });
}

/**
 * 编辑内容设置
 * 作者：糜家智
 * 时间：2021/8/12 18:09
 */
export async function editSetting(data) {
  return request('/admin/cms.Setting/edit', {
    method: 'POST',
    data,
  });
}

/**
 * 作者：糜家智
 * 时间：2022/5/18
 * 功能：内容回收站
 */
export async function recoverList(params) {
  return request('/admin/cms.Content/recover', {
    method: 'GET',
    params: { ...params },
  });
}

/**
 * 作者：糜家智
 * 时间：2022/5/18
 * 功能：恢复内容
 */
export async function recoverReco(data) {
  return request('/admin/cms.Content/recoverReco', {
    method: 'POST',
    data,
  });
}

/**
 * 作者：糜家智
 * 时间：2022/5/18
 * 功能：恢复删除（彻底删除）
 */
export async function recoverDel(data) {
  return request('/admin/cms.Content/recoverDele', {
    method: 'POST',
    data,
  });
}




