/* eslint-disable */
import { request } from 'umi';

/**
 * 接口列表
 * 作者：糜家智
 * 时间：2021/7/26 18:07
 */
export async function getApiList(params, options) {
  return request('/admin/Api/list', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 添加Api
 * 作者：糜家智
 * 时间：2021/7/26 18:07
 */
export async function addApi(data) {
  return request('/admin/Api/add', {
    method: 'POST',
    data,
  });
}

/**
 * 编辑Api
 * 作者：糜家智
 * 时间：2021/7/26 18:07
 */
export async function editApi(data) {
  return request('/admin/Api/edit', {
    method: 'POST',
    data,
  });
}

/**
 * 删除Api
 * 作者：糜家智
 * 时间：2021/7/26 18:07
 */
export async function delApi(data) {
  return request('/admin/Api/dele', {
    method: 'POST',
    data,
  });
}

/**
 * 获取地区
 * 作者：糜家智
 * 时间：2021/7/26 18:07
 */
export async function getRegionList(params, options) {
  return request('/admin/Region/list', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 地区信息
 * @param {array} params 请求参数
 */
export function getRegionInfo(params, options) {
  return request({
    url: '/admin/Region/info',
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 地区添加
 * @param {array} data 请求数据
 */
export function addRegion(data) {
  return request({
    url: '/admin/Region/add',
    method: 'post',
    data,
  });
}
/**
 * 地区修改
 * @param {array} data 请求数据
 */
export function editRegion(data) {
  return request({
    url: '/admin/Region/edit',
    method: 'post',
    data,
  });
}
/**
 * 地区删除
 * @param {array} data 请求数据
 */
export function delRegion(data) {
  return request({
    url: '/admin/Region/dele',
    method: 'post',
    data,
  });
}

/**
 * 清除缓存
 * @param {array} data 请求数据
 */
export async function clearCache(data) {
  return request('/admin/AdminSetting/cacheClear', {
    method: 'POST',
    data,
  });
}

/**
 * 小程序信息
 * @param {array} data 请求数据
 */
export async function getMiniInfo(params, options) {
  return request('/admin/SettingWechat/miniInfo', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 小程序修改
 * @param {array} data 请求数据
 */
export async function miniEdit(data) {
  return request('/admin/SettingWechat/miniEdit', {
    method: 'POST',
    data,
  });
}

/**
 * 公众号信息
 * @param {array} data 请求数据
 */
export async function getOffiInfo(params, options) {
  return request('/admin/SettingWechat/offiInfo', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 公众号修改
 * @param {array} data 请求数据
 */
export async function offiEdit(data) {
  return request('/admin/SettingWechat/offiEdit', {
    method: 'POST',
    data,
  });
}
