/* eslint-disable */
import { request } from 'umi';

/**
 * 获取用户列表
 * 作者：糜家智
 * 时间：2021/7/26 18:07
 */
export async function getMemberList(params, options) {
  return request('/admin/Member/list', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 禁用和启用
 * @param params
 * @param options
 * @returns {Promise<*>}
 */
export async function disable(params, options) {
  return request('/admin/Member/disable', {
    method: 'POST',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 添加用户
 * @param params
 * @param options
 * @returns {Promise<*>}
 */
export async function addUser(params, options) {
  return request('/admin/Member/add', {
    method: 'POST',
    data: { ...params },
    ...(options || {}),
  });
}

/**
 * 编辑用户
 * @param params
 * @param options
 * @returns {Promise<*>}
 */
export async function editUser(params, options) {
  return request('/admin/Member/edit', {
    method: 'POST',
    data: { ...params },
    ...(options || {}),
  });
}

/**
 * 用户详情
 * @param params
 * @param options
 * @returns {Promise<*>}
 */
export async function getUserInfo(params, options) {
  return request('/admin/Member/info', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 删除用户
 * @param params
 * @param options
 * @returns {Promise<*>}
 */
export async function delUser(params, options) {
  return request('/admin/Member/dele', {
    method: 'POST',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 编辑用户日志
 * @param params
 * @param options
 * @returns {Promise<*>}
 */
export async function getMemberLog(params, options) {
  return request('/admin/MemberLog/list', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

/**
 * 欢迎页项目
 * @param {array} params 请求参数
 */
export async function projectList() {
  return request('/admin/index/welcomeList', {
    method: 'get',
  });
}
