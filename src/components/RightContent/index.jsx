import React, { useState } from 'react';
import { Space, Tooltip, message } from 'antd';
import { QuestionCircleOutlined, DeleteOutlined, SyncOutlined } from '@ant-design/icons';
import { useModel } from 'umi';
import Avatar from './AvatarDropdown';
import HeaderSearch from '../HeaderSearch';
import styles from './index.less';
import { clearCache, getApiList } from '@/services/setting';

const GlobalHeaderRight = () => {
  const { initialState } = useModel('@@initialState');
  const [isShowClear, setIsShowClear] = useState(true);

  if (!initialState || !initialState.settings) {
    return null;
  }

  const { navTheme, layout } = initialState.settings;
  let className = styles.right;

  if ((navTheme === 'dark' && layout === 'top') || layout === 'mix') {
    className = `${styles.right}  ${styles.dark}`;
  }

  const onClearCache = async () => {
    setIsShowClear(false);
    const res = await clearCache();

    if (res.code == 200) {
      message.success('缓存清除成功~');
      setIsShowClear(true);
    }
  };

  return (
    <Space className={className}>
      <HeaderSearch
        className={`${styles.action} ${styles.search}`}
        placeholder="站内搜索"
        defaultValue="miAdmin"
        options={[
          {
            label: '',
            value: '',
          },
        ]}
      />

      <span
        className={styles.action}
        onClick={() => {
          window.open('https://miren123.gitee.io/mi-admin-docs/');
        }}
      >
        <QuestionCircleOutlined />
      </span>

      <Avatar />

      {isShowClear ? (
        <Tooltip placement="bottomRight" color={'cyan'} autoAdjustOverflow={true} title="清除缓存">
          <DeleteOutlined className={styles.clearBtn} onClick={() => onClearCache()} />
        </Tooltip>
      ) : (
        <SyncOutlined spin />
      )}
    </Space>
  );
};

export default GlobalHeaderRight;
